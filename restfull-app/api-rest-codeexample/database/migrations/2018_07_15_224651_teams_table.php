<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::dropIfExists('teams');
        Schema::create('teams', function (Blueprint $table) {
        	$table->increments('id');
        	$table->string('caption');
        	$table->string('logo');
        	$table->integer('year_founded');
        	$table->string('website');
        	$table->string('short_caption');
        	$table->string('about');        		
        	$table->integer('current_division_id');
        	$table->integer('arena_id');       		
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('teams');
    }
}
