<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    //
	protected $primaryKey = 'id';
	protected $fillable = array('caption', 'logo', 'year_founded', 'website','short_caption','about','current_division_id','arena_id','updated_at','created_at');
	
	protected $hidden = [
	];
	
}
