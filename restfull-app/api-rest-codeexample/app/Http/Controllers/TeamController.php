<?php

namespace App\Http\Controllers;
use App\Teams;

use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    	$teams=Teams::all();
    	return $teams;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    	try{
    		$team = new Teams;
    		$team->website = $request->get('website');
    		$team->about = $request->get('about');
    		$team->caption = $request->get('caption');
    		$team->logo ='logo';
    		$team->year_founded =2018;
    		$team->short_caption ='short_caption';
    		$team->about ='about';
    		$team->current_division_id =5;
    		$team->arena_id =5;    		
    		$team->save();
    		return $team;
    	}
    	catch(\Exception $e){
    		\Log::info('Error store team: '.$e);
    		return \Response::json(['stored' => false], 500);
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    	try{
    		$team = Teams::find($id);
    		return $team;
    	}
    	catch(\Exception $e){
    		\Log::info('Error showed team: '.$e);
    		return \Response::json(['show' => false], 500);
    	}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    	try{
    		$team = Teams::find($id);
    		$team->fill($request->all())->save();
    		return $team;
    	}
    	catch(\Exception $e){
    		\Log::info('Error updated team: '.$e);
    		return \Response::json(['update' => false], 500);
    	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    	try {
    		$team = Teams::findOrFail($id);
    		$team->delete();
    	}
    	catch(\Exception $e){
    		\Log::info('Error deleted team: '.$e);
    		return \Response::json(['delete' => false], 500);
    	}
    }
}
