import {Component, OnInit,ElementRef, AfterViewInit} from '@angular/core';
import { Mostwatched } from '../class/Mostwatched';
import { MOSTWATCHED } from '../mock/mock-Mostwatched';
import { IndexService }  from '../service/index/index.service';

declare var Swiper: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit , AfterViewInit  {

  data:Mostwatched;
  moviesatched:any[];
  moviesrecently:any[];
  bestmovies:any[];

  constructor(private elementRef: ElementRef,
              private indexService: IndexService) { }

  ngOnInit() {
      this.setMostwatched();
      this.getMostwatched();
      this.getRecentlyAdded();
      this.getBestMovies();
  }

  /*Set objects*/
  setMostwatched(): void {
     this.indexService.setMostwatched()
         .subscribe(data=> {
             this.data = data;
     });
  }

  /*Most Watched*/
  getMostwatched(): void {
    this.indexService.getMostwatched()
    .subscribe(mostwatched =>{
         this.moviesatched= mostwatched['results'];
       });
  }

  /*Recently added*/
  getRecentlyAdded(): void {
    this.indexService.getRecentlyAdded()
    .subscribe(recentlyadded =>{
        this.moviesrecently = recentlyadded['results'];
       });
  }

  /*Recently added*/
  getBestMovies(): void {
    this.indexService.getBestMovies()
    .subscribe(bestmovies =>{
        this.bestmovies = bestmovies['results'];
       });
  }

  ngAfterViewInit() {
      var swiper = new Swiper('.homeslider > .swiper-container', {
      pagination: '.swiper-pagination',
      paginationClickable: true,
      preventClicks:false,
      preventClicksPropagation:false,
      effect:'fade',
      breakpoints: {
        320: {
          height:200
        },

        480: {
          height:300
        },

        768: {
          height:400
        },
        1024: {
          height:500
        }
      }
    });

    var recentswiper = new Swiper('.recentslider > .swiper-container', {
      nextButton: '.recent-next',
      prevButton: '.recent-prev',
      slidesPerView: 8,
      paginationClickable: true,
      preventClicks:false,
      preventClicksPropagation:false,
      spaceBetween: 10,
      breakpoints: {
        320: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        480: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        768: {
          slidesPerView: 5,
          spaceBetween: 5
        },
        1024: {
          slidesPerView: 6,
          spaceBetween: 10
        }
      }
    });

    var mostswiper = new Swiper('.mostslider > .swiper-container', {
      nextButton: '.most-next',
      prevButton: '.most-prev',
      slidesPerView: 8,
      paginationClickable: true,
      preventClicks:false,
      preventClicksPropagation:false,
      spaceBetween: 10,
      breakpoints: {
        320: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        480: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        768: {
          slidesPerView: 5,
          spaceBetween: 5
        },
        1024: {
          slidesPerView: 6,
          spaceBetween: 10
        }
      }
    });

    var topswiper = new Swiper('.topslider > .swiper-container', {
      nextButton: '.top-next',
      prevButton: '.top-prev',
      slidesPerView: 8,
      paginationClickable: true,
      preventClicks:false,
      preventClicksPropagation:false,
      spaceBetween: 10,
      breakpoints: {
        320: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        480: {
          slidesPerView: 3,
          spaceBetween: 5
        },

        768: {
          slidesPerView: 5,
          spaceBetween: 5
        },
        1024: {
          slidesPerView: 6,
          spaceBetween: 10
        }
      }
    });
  }

}
