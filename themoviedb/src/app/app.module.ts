import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { GenreComponent } from './genre/genre.component';
import { AppRoutingModule } from './/app-routing.module';
import { YearComponent } from './year/year.component';
import { IndexComponent } from './index/index.component';
import { LanguageComponent } from './language/language.component';
import { MostwatchedComponent } from './mostwatched/mostwatched.component';
import { SingleComponent } from './single/single.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { ContactComponent } from './contact/contact.component';
import { TermsComponent } from './terms/terms.component';
import { IndexService } from './service/index/index.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule }    from '@angular/common/http';
import { MessageService } from './service/message/message.service';
import { GenreService } from './service/genre/genre.service';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    GenreComponent,
    YearComponent,
    IndexComponent,
    LanguageComponent,
    MostwatchedComponent,
    SingleComponent,
    ContactComponent,
    TermsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    NgxPaginationModule
  ],
  providers: [
    IndexService,
    MessageService,
    GenreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
