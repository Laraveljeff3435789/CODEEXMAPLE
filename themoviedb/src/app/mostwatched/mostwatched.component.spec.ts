import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostwatchedComponent } from './mostwatched.component';

describe('MostwatchedComponent', () => {
  let component: MostwatchedComponent;
  let fixture: ComponentFixture<MostwatchedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostwatchedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostwatchedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
