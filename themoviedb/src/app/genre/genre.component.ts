import {Component, OnInit,ElementRef} from '@angular/core';
import { GenreService }  from '../service/genre/genre.service';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {

    genre:any[];
    p:any;

    constructor(private genreService: GenreService) { }

    ngOnInit() {
        this.getGenre();
    }

    /*Most getGenre*/
    getGenre(): void {
      this.genreService.getGenre()
      .subscribe(genre =>{
          this.genre= genre['results'];
      });
    }
}
