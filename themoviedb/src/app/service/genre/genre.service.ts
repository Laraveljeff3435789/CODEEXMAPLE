import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ValidationService } from '../validation/validation.service';
import { MessageService } from '../message/message.service';
import { Validation } from '../../validation/validation';
/*import { Mostwatched , Results } from '../../class/Mostwatched';
import { MOSTWATCHED  } from '../../mock/mock-Mostwatched';*/


@Injectable({
  providedIn: 'root'
})
export class GenreService {

    private genredUrl = environment.protocol+'://'+environment.ApiUrl+'/3/discover/movie?api_key='+environment.ApiKey+'&with_genres=18&sort_by=vote_average.desc&vote_count.gte=20';

    /**Constructor to inject dependenciess**/
    constructor(  private http: HttpClient,
                  private validationService :ValidationService){
    }

    /** Bring the server data */
    getGenre (): Observable<any[]> {
      const httpOptions = {
        headers: new HttpHeaders(
            {
              'Content-Type': 'application/json'
            }
          )
      };
      return this.http.get<any[]>(this.genredUrl,httpOptions)
        .pipe(
          tap(movies => this.validationService.log(`Retrieve genre`)),
          catchError(this.validationService.handleError('getGenre', []))
        );
    }
}
