import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ValidationService } from '../validation/validation.service';
import { MessageService } from '../message/message.service';
import { Validation } from '../../validation/validation';
import { Mostwatched , Results } from '../../class/Mostwatched';
import { MOSTWATCHED  } from '../../mock/mock-Mostwatched';

@Injectable({
  providedIn: 'root'
})
export class IndexService {
    /*links url Api */ /*Most Watched*/
    private mostwatchedUrl = environment.protocol+'://'+environment.ApiUrl+'/3/discover/movie?api_key='+environment.ApiKey+'&sort_by=popularity.desc';
    private recentlyUrl = environment.protocol+'://'+environment.ApiUrl+'/3/discover/movie?api_key='+environment.ApiKey+'&certification_country=US&certification=R&sort_by=vote_average.desc';
    private bestMovicesUrl = environment.protocol+'://'+environment.ApiUrl+'/3/discover/movie?api_key='+environment.ApiKey+'&primary_release_year=2010&sort_by=vote_average.desc';

    /**Constructor to inject dependenciess**/
    constructor(  private http: HttpClient,
                  private validationService :ValidationService){
    }

    /** Set object*/
     setMostwatched(): Observable<Mostwatched> {
        return of(new Mostwatched);
    }

    /** Bring the server data */
    getMostwatched (): Observable<Mostwatched[]> {
      const httpOptions = {
        headers: new HttpHeaders(
            {
              'Content-Type': 'application/json'
            }
          )
      };
      return this.http.get<Mostwatched[]>(this.mostwatchedUrl,httpOptions)
        .pipe(
          tap(movies => this.validationService.log(`Retrieve Mostwatched`)),
          catchError(this.validationService.handleError('getMostwatched', []))
        );
    }

    /** Bring the server data */
    getRecentlyAdded (): Observable<Mostwatched[]> {
      const httpOptions = {
        headers: new HttpHeaders(
            {
              'Content-Type': 'application/json'
            }
          )
      };
      return this.http.get<Mostwatched[]>(this.recentlyUrl,httpOptions)
        .pipe(
          tap(movies => this.validationService.log(`Retrieve RecentlyAdded`)),
          catchError(this.validationService.handleError('getRecentlyAdded', []))
        );
    }

    /** Bring the server data */
    getBestMovies(): Observable<Mostwatched[]> {
      const httpOptions = {
        headers: new HttpHeaders(
            {
              'Content-Type': 'application/json'
            }
          )
      };
      return this.http.get<Mostwatched[]>(this.bestMovicesUrl,httpOptions)
        .pipe(
          tap(movies => this.validationService.log(`Best Movies`)),
          catchError(this.validationService.handleError('getBestMovies', []))
        );
    }

}
