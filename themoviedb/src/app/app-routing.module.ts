import { NgModule } from '@angular/core';
import { RouterModule, Routes  , PreloadAllModules } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GenreComponent } from './genre/genre.component';
import { YearComponent } from './year/year.component';
import { IndexComponent } from './index/index.component';
import { LanguageComponent } from './language/language.component';
import { MostwatchedComponent } from './mostwatched/mostwatched.component';
import { SingleComponent } from './single/single.component';
import { TermsComponent } from './terms/terms.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  { path: 'home', component: IndexComponent },
  { path: 'gener', component: GenreComponent },
  { path: 'year', component: YearComponent },
  { path: 'language', component: LanguageComponent },
  { path: 'mostwatched', component: MostwatchedComponent },
  { path: 'single', component: SingleComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'contact', component: ContactComponent },
  { path: '*', component: IndexComponent },
  { path: '**', component: IndexComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }) ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
