<div class="row">
    <div class="form-group form-group-label col-sm-3">
        <label class="control-label">
            <?php echo $this->_tpl_vars['Col']->getCaption(); ?>

        </label>
    </div>

    <div class="form-group col-sm-9">
        <div class="form-control-static">
            <?php echo $this->_tpl_vars['Col']->getDisplayValue($this->_tpl_vars['Renderer']); ?>

        </div>
    </div>
</div>