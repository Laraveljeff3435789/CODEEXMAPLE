<form id="<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo $this->_tpl_vars['Grid']['FormAction']; ?>
">

    <?php if (! $this->_tpl_vars['isEditOperation'] && $this->_tpl_vars['Grid']['AllowAddMultipleRecords']): ?>
        <div class="btn-group pull-right form-collection-actions">
            <button type="button" class="btn btn-default icon-copy js-form-copy" title="Copy"></button>
            <button type="button" class="btn btn-default icon-remove js-form-remove" style="display: none" title="Delete"></button>
        </div>
    <?php endif; ?>
    <div class="clearfix"></div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/messages.tpl', 'smarty_include_vars' => array('type' => 'danger','dismissable' => true,'messages' => $this->_tpl_vars['Grid']['ErrorMessages'],'displayTime' => $this->_tpl_vars['Grid']['MessageDisplayTime'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/messages.tpl', 'smarty_include_vars' => array('type' => 'success','dismissable' => true,'messages' => $this->_tpl_vars['Grid']['Messages'],'displayTime' => $this->_tpl_vars['Grid']['MessageDisplayTime'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    
    <div class="row">
        <?php $this->assign('Columns', $this->_tpl_vars['Grid']['FormLayout']->getColumns()); ?>
        <div style="margin: 0px 20px;">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#about-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="about-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">About</a></li>
            <li role="presentation"><a href="#add-info-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="add-info-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">Additional info</a></li>
            <li role="presentation"><a href="#media-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="media-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">Media</a></li>
          </ul>

          <div class="tab-content" style="margin-top: 20px; min-height: 350px">
            <div role="tabpanel" class="tab-pane active" id="about-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['original_title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['tagline'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['overview'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="add-info-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['release_date'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['original_language_id'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['genre_id'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['runtime'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['rating'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="media-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['poster'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['trailer'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
          </div>
        </div>
    </div>

    <?php $_from = $this->_tpl_vars['HiddenValues']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['HiddenValueName'] => $this->_tpl_vars['HiddenValue']):
?>
        <input type="hidden" name="<?php echo $this->_tpl_vars['HiddenValueName']; ?>
" value="<?php echo $this->_tpl_vars['HiddenValue']; ?>
" />
    <?php endforeach; endif; unset($_from); ?>

    
    <?php if ($this->_tpl_vars['flashMessages']): ?>
        <input type="hidden" name="flash_messages" value="1" />
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12 form-error-container"></div>
    </div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'forms/form_scripts.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</form>