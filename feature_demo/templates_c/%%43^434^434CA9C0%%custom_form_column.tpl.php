<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'custom_templates/custom_form_column.tpl', 3, false),)), $this); ?>
<?php $this->assign('ColumnViewData', $this->_tpl_vars['Col']->getViewData()); ?>
<?php $this->assign('Editor', $this->_tpl_vars['ColumnViewData']['EditorViewData']['Editor']); ?>
<?php $this->assign('editorId', ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Grid']['FormId'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_') : smarty_modifier_cat($_tmp, '_')))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Editor']->getName()) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Editor']->getName()))); ?>

<div class="form-group form-group-label col-sm-4">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'forms/field_label.tpl', 'smarty_include_vars' => array('editorId' => $this->_tpl_vars['editorId'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

<div class="form-group col-sm-8">
    <div class="col-input" style="width:100%;max-width:<?php echo $this->_tpl_vars['Editor']->getMaxWidth(); ?>
" data-column="<?php echo $this->_tpl_vars['ColumnViewData']['FieldName']; ?>
">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ((is_array($_tmp=((is_array($_tmp='editors/')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Editor']->getEditorName()) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Editor']->getEditorName())))) ? $this->_run_mod_handler('cat', true, $_tmp, '.tpl') : smarty_modifier_cat($_tmp, '.tpl')), 'smarty_include_vars' => array('Editor' => $this->_tpl_vars['Editor'],'ViewData' => $this->_tpl_vars['ColumnViewData']['EditorViewData'],'FormId' => $this->_tpl_vars['Grid']['FormId'],'id' => $this->_tpl_vars['editorId'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <?php if (! $this->_tpl_vars['Grid']['FormLayout']->isHorizontal() && $this->_tpl_vars['Editor']->isInlineLabel()): ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'forms/field_label.tpl', 'smarty_include_vars' => array('editorId' => $this->_tpl_vars['editorId'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php endif; ?>
    </div>
</div>