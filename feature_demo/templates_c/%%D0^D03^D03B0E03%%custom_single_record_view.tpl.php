<div id="pgui-view-grid">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_header.tpl", 'smarty_include_vars' => array('pageTitle' => $this->_tpl_vars['Grid']['Title'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="<?php if ($this->_tpl_vars['Grid']['FormLayout']->isHorizontal()): ?>form-horizontal<?php endif; ?>">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "forms/actions_view.tpl", 'smarty_include_vars' => array('top' => true,'isHorizontal' => $this->_tpl_vars['Grid']['FormLayout']->isHorizontal())));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="row">
            <div class="col-md-12 js-message-container"></div>
            <div class="clearfix"></div>
            <div class="form-static <?php if ($this->_tpl_vars['Grid']['FormLayout']->isHorizontal()): ?>form-horizontal col-lg-8<?php else: ?>col-md-8 col-md-offset-2<?php endif; ?>">
                <!--<Custom code>-->
                <?php $this->assign('Columns', $this->_tpl_vars['Grid']['FormLayout']->getColumns()); ?>
                <div class="demo-tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#common-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="common-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">
                            Common
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#dimensions-display-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="dimensions-display-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">
                            Dimensions & Display
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#hardware-software-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="hardware-software-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">
                            Hardware & Software
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#camera-battery-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" aria-controls="camera-battery-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
" role="tab" data-toggle="tab">
                            Camera & Battery
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="common-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['model_name'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['release_year'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['release_month'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['colors'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['photo'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['photo_back'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dimensions-display-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['height'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['length'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['width'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['weight'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['display_type'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['display_size'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['display_resolution_x'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['display_resolution_y'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="hardware-software-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['chipset'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['cpu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['gpu'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['storage_min'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['storage_max'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['os_basic'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['os_upgradable'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['web_browser'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="camera-battery-<?php echo $this->_tpl_vars['Grid']['FormId']; ?>
">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['camera_resolution'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['camera_video_max_x'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['camera_video_max_y'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['battery_type'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['battery_standby_max_time'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['battery_talk_max_time'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "custom_templates/custom_form_view_column.tpl", 'smarty_include_vars' => array('Col' => $this->_tpl_vars['Columns']['battery_music_play_max_time'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                </div>
                </div>
                <!--</Custom code>-->
            </div>
        </div>

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "forms/actions_view.tpl", 'smarty_include_vars' => array('top' => false,'isHorizontal' => $this->_tpl_vars['Grid']['FormLayout']->isHorizontal())));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </div>
</div>