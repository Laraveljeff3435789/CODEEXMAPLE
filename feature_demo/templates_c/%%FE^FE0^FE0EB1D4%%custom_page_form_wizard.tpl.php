<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "page_header.tpl", 'smarty_include_vars' => array('pageTitle' => $this->_tpl_vars['Grid']['Title'],'pageWithForm' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="col-md-12 js-form-container" data-form-url="<?php echo $this->_tpl_vars['Grid']['FormAction']; ?>
&flash=true">

    <div class="row">
        <div class="js-form-collection <?php if ($this->_tpl_vars['Grid']['FormLayout']->isHorizontal()): ?>col-lg-8<?php else: ?>col-md-8 col-md-offset-2<?php endif; ?>">
            <?php $_from = $this->_tpl_vars['Forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['forms'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['forms']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['Form']):
        $this->_foreach['forms']['iteration']++;
?>
                <?php echo $this->_tpl_vars['Form']; ?>

                <?php if (! ($this->_foreach['forms']['iteration'] == $this->_foreach['forms']['total'])): ?><hr><?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
        </div>
    </div>

    
    <div class="form-actions row">
        <div class="col-md-12">
            <div class="row">
                <div class="<?php if ($this->_tpl_vars['isHorizontal']): ?>col-sm-9 col-sm-offset-3<?php else: ?>col-md-8 col-md-offset-2<?php endif; ?>">
                    <div class="btn-toolbar">
                        <a class="btn btn-default" href="<?php echo $this->_tpl_vars['Grid']['CancelUrl']; ?>
"><?php echo $this->_tpl_vars['Captions']->GetMessageString('Cancel'); ?>
</a>
                        <a href="#" class="btn btn-primary js-previous" disabled="disabled"><?php echo $this->_tpl_vars['Captions']->GetMessageString('Previous'); ?>
</a>
                        <a href="#" class="btn btn-primary js-next"><?php echo $this->_tpl_vars['Captions']->GetMessageString('Next'); ?>
</a>
                        <button type="submit" class="btn btn-primary js-save js-primary-save" data-action="open" data-url="<?php echo $this->_tpl_vars['Grid']['CancelUrl']; ?>
" style="display: none">
                            <?php echo $this->_tpl_vars['Captions']->GetMessageString('Save'); ?>

                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
</div>

