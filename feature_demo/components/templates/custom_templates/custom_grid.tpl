{capture assign=GridBeforeFilterStatus}
<div class="pull-right panel panel-primary">
    <div class="panel-heading">
    <span class="panel-title">Life Expectancy</span>
    </div>
    <div class="panel-body">
        <form id="custom-filter-form" class="form-inline" method="get">
            <div class="form-group">
                <label for="minLifeExpectancy">Min age</label>
                <input type="number" id="minLifeExpectancy" class="form-control input-sm" name="minLifeExpectancy" value="{$minLifeExpectancy}" min="40" max="90">
            </div>
            <div class="form-group">
                <label for="maxLifeExpectancy">Max age</label>
                <input type="number" id="maxLifeExpectancy" class="form-control input-sm" name="maxLifeExpectancy" value="{$maxLifeExpectancy}" min="40" max="90">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Apply</button>
        </form>
    </div>
</div>
{/capture}

{if $Grid->GetViewMode() == 0}
    {include file='list/grid_table.tpl'}
{else}
    {include file='list/grid_card.tpl'}
{/if}
