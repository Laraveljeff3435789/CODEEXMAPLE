<?php

return array(

    'Data Grid' => 'Pages showing common data grid features.',

    'Grid Columns' => 'Pages that illustrate column-specific features such as formatting, hyperlinks, etc.',

    'Grid Options' => 'Examples for a number data grid options.',

    'Master-Detail Views' => 'Examples of various kinds of master/detail presentations.',

    'Data Input Forms' => 'Pages showing common Edit and Insert form features including custom templates.',

    'Editors' => 'Each page is dedicated to a certain edit control provided by PHP Generator.',

    'Data Filtering' => 'Provides examples of usage of built-in and user-defined data filtering tools.',

    'Sorting' => 'Learn how to sort your data in various ways.',

    'Partitioning' => 'Provides examples of all kinds of data partitioning supported by PHP Generator.',

    'Exporting & Printing' => 'Learn more about exporting and printing possibilities.',

    'Charts' => 'Charts are one of the most intuitive way to display the data. Study the pages below to learn how create and customize your charts.',

    'Many-to-Many Relations' => 'Learn how to manage many-to-many relationships with PHP Generator.',

    'Fine-tuning & Tweaking' => 'Think you are familiar with all the features of PHP Generator? OK, now it is time to study the pages in this group.',

    'What\'s new' => 'Pages showing features added or significantly updated in the latest major version of PHP Generator (currently 16.9) and its subreleases.',

    'Custom Templates' => 'Customize everything according to your dreams.',

    'Data Sources' => 'Learn more about data sources your pages can be based on.',

    'Emailing' => 'Provides examples of sending emails from server-side events.',

    'Recently added/updated' => 'Provides quick access to recently added or significantly updated pages. These pages are also included into the appropriate groups.',

    'Calculated Columns' => 'Provides examples of using calculated (AKA computed) columns.',

    'Image Management' => 'Learn more about image management features provided by PHP Generator.'

);