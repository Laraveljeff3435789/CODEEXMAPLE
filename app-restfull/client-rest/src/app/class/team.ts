  export class Team {
    id: string;
    caption: string;
    logo: string;
    year_founded: string;
    website: string;
    short_caption: string;
    about: string;
    current_division_id: string;
    arena_id: string;
    updated_at: string;
    created_at: string;
}
