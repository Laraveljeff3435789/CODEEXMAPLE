/*
*Este clase obtiene todas las validaciones
*Author : Jefferson Jose Medina Cedeño
*Date   :2018-04-19
*/
export class Validation {

    constructor(){
    }
  /**
   * Funcion para validar la veracidad de un correo
   * @param str - cadena que contiene el correo
   * @param result - retorna verdadero si es un correo valido
   */
   validateEmail(str: string): boolean {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(str).toLowerCase());
   }
  /**
   * Funcion valida si una cadena esta vacia
   * @param str - cadena que se pasa como parametro
   * @param result retorna verdadero si esta vacia
   */
   isNullOrEmpty(str: string): boolean {
    str = str.trim();
    if (!str) {
      return false;
    }
    return true;
  }

}
