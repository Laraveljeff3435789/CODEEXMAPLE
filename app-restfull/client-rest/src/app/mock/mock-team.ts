import { Team } from '../class/team';

export const TEAM: Team[] = [
  {
    id: '',
    caption: '',
    logo: '',
    year_founded: '',
    website: '',
    short_caption: '',
    about: '',
    current_division_id: '',
    arena_id: '',
    updated_at: '',
    created_at: ''
  }
];
