import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message/message.service';
import { Validation } from '../../validation/validation';


@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  validation :Validation;

  constructor(private messageService: MessageService) {
    this.validation= new Validation();
  }
  /*
  * Personalizacion de los mensajes
  * @param str - cadena que contiene el correo
  * @param result - retorna verdadero si es un correo valido sino falso
  */
  public validateEmail(str: string) {
    if(!this.validation.validateEmail(str)){
      this.messageService.add('Debe digitar un correo valido');
      return false;
    }
    return true;
  }
  /*
   * Personalizacion de los mensajes de error
   * @param str - cadena que contiene el correo
   * @param result - retorna verdadero si no esta vacio , sino falso
   */
  public isNullOrEmpty(str: string) {
   if(!this.validation.isNullOrEmpty(str)){
      this.messageService.add('Este campo no puede ir vacio, debe diligenciarlo');
      return false;
    }
    return true;
  }
  /**
   * Manejar la operación Http que falló.
   * Deja que la aplicación continúe.
   * @param operation -nombre de la operación que falló
   * @param result - valor opcional para devolver como el resultado observable
   */
    public handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        // TODO: enviar el error a la infraestructura de registro remoto
        console.error(error); // log a consola

        // TODO: mejor trabajo de transformar el error para el consumo del usuario
        this.log(`${operation} failed: ${error.message}`);

        // Deje que la aplicación siga funcionando devolviendo un resultado vacío.
        return of(result as T);
      };
    }
    /** Log para mostrar mensajes internos */
    public log(message: string) {
      this.messageService.clear();
      this.messageService.add('Mensaje: ' + message);
    }
}
