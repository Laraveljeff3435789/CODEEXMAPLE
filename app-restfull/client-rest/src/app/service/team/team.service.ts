import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ValidationService } from '../validation/validation.service';
import { Team } from '../../class/team';
import { TEAM } from '../../mock/mock-team';


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private teamUrl = environment.protocol+'://'+environment.ApiUrl+'/api/teams';
  private teamUrllDelete = environment.protocol+'://'+environment.ApiUrl+'/api/teams';

  constructor( private http: HttpClient,private validationService :ValidationService) { }

  /** Permite setear teams*/
  setTeams(): Observable<Team> {
     return of(new Team);
  }

  /** Trae los datos del servidor */
  getTeams (): Observable<Team[]> {
    const httpOptions = {
      headers: new HttpHeaders(
          {
            'Content-Type': 'application/json'
          }
        )
   };
   return this.http.get<Team[]>(this.teamUrl,httpOptions)
       .pipe(
           tap(teams => this.validationService.log(`trae teams`)),
           catchError(this.validationService.handleError('getTeams', []))
       );
   }

   //////// Metodos para crud //////////

   /** POST: agrega un team  al servidor */
   addTeam (team: Team): Observable<Team> {
     const httpOptions = {
       headers: new HttpHeaders(
           {
             'Content-Type': 'application/json'
           }
         )
     };
     return this.http.post<Team>(this.teamUrl, team, httpOptions).pipe(
       tap((team: Team) => this.validationService.log(`Agrega team w/ id=${team.id}`)),
       catchError(this.validationService.handleError<Team>('addTeam'))
     );
   }

   /** PUT: actualiza team en el servidor */
  updateTeam (team: Team): Observable<any> {
    const id = typeof team === 'number' ? team : team.id;
    const url = `${this.teamUrl}/${id}?website=${team.website}&about=${team.about}&caption=${team.caption}`;

    const httpOptions = {
      headers: new HttpHeaders(
          {
            'Content-Type': 'application/json'
          }
        )
    };
    return this.http.put(url, httpOptions).pipe(
      tap(_ => this.validationService.log(`updated team id=${team.id}`)),
      catchError(this.validationService.handleError<any>('updateTeam'))
    );
  }

  /* GET teams por nombre */
  searchTeam(term: string): Observable<Team[]> {
    const httpOptions = {
      headers: new HttpHeaders(
          {
            'Content-Type': 'application/json'
          }
        )
    };
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Team[]>(`${this.teamUrl}/?name=${term}`,httpOptions).pipe(
      tap(_ => this.validationService.log(`Encuentra el team buscado"${term}"`)),
      catchError(this.validationService.handleError<Team[]>('searchTeam', []))
    );
  }

  /** DELETE: Borra team del servidor */
  deleteTeam (team: Team | number): Observable<Team> {
    const id = typeof team === 'number' ? team : team.id;
    const url = `${this.teamUrllDelete}/${id}`;
    const httpOptions = {
      headers: new HttpHeaders(
          {
            'Content-Type': 'application/json'
          }
        )
    };
    return this.http.delete<Team>(url, httpOptions).pipe(
      tap(_ => this.validationService.log(`Borrar team  id=${id}`)),
      catchError(this.validationService.handleError<Team>('deleteTeam'))
    );
  }


}
