import { Component, OnInit , Input} from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormGroup, FormBuilder, Validators ,FormControl } from '@angular/forms';
import { TEAM } from '../mock/mock-team';
import { Team } from '../class/team';
import { TeamService } from '../service/team/team.service';
import { Validation } from '../validation/validation';
import { ValidationService } from '../service/validation/validation.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: [
            '../../assets/css/main_facebook.css',
            './team.component.css'
  ]
})

export class TeamComponent implements OnInit {

  teams: Team[];
  display='none';
  displayDelete='none';
  displayEdit='none';
  idDelete : string;

  form: FormGroup;
  data: Team;

  /*Llamada a metodo de consumo de menus*/
  constructor(private teamService: TeamService,
              private validationService :ValidationService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.getTeam();
      this.setTeam();
      this.form = this.formBuilder.group({
        website: [null, Validators.required],
        about: [null, Validators.required],
        caption: [null, Validators.required]
      });
  }

  /*Metodo usado para setear usuarios*/
  setTeam(): void {
   this.teamService.setTeams()
       .subscribe(data=> this.data = data);
  }

  /**Get Teams list*/
  getTeam():void {
    this.teamService.getTeams()
    .subscribe( teams => { this.teams = teams; });
  }

  /*Metodo abre dialogo para Mostrar formmulario*/
  openModalAdd(){
      this.display='block';
  }

  /*Metodo abre dialogo para Mostrar formulario*/
  onCloseHandledAdd(){
       this.display='none';
  }

  /*Metodo abre dialogo para Mostrar formmulario de editar*/
  openModalEdit(team : Team){
      this.displayEdit='block';
      this.data = team;
  }

  /*Metodo abre dialogo para Mostrar formulario de editar*/
  onCloseHandledEdit(){
       this.displayEdit='none';
  }

  /*Metodo accion para agregar usuarios*/
  add(): void {
      this.teamService.addTeam(this.data)
        .subscribe(team => {
          this.teams.push(team);
      });
  }

  edit(team : Team): void{
    this.teamService.updateTeam(team)
      .subscribe(team => {
          var index = this.teams.findIndex(obj => obj.id== team.id);
          this.teams[index] = team
      });
  }

  /*Se inician metodos de validacion*/
  isFieldValid(field: string) {
  return !this.form.get(field).valid && this.form.get(field).touched;
  }

  /**Envia datos de usuarios*/
  displayFieldCss(field: string) {
  return {
    'has-error': this.isFieldValid(field),
    'has-feedback': this.isFieldValid(field)
   };
  }

  /*Envia datos*/
  onSubmit() {
    if (this.form.valid) {
      this.add();
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  /*Envia datos*/
  onSubmitEdit(team : Team) {
    if (this.form.valid) {
      this.edit(team);
    } else {
      this.validateAllFormFields(this.form);
    }
  }


  /*Valida campos de formulario*/
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
  }

  /*Limpia formulario*/
  reset() {
    this.form.reset();
  }

  /*Metodo abre dialogo de confirmacion de borrado*/
  openModalDelete(id:string){
      this.displayDelete='block';
      this.idDelete=id;
  }

  /*Metodo cierra dialogo de confirmacion de borrado*/
  onCloseHandledDelete(){
       this.displayDelete='none';
  }

  /*Metodo accion para borrar usuarios*/
  delete(id: string): void {
     this.teamService.deleteTeam( { id } as Team ).
         subscribe( team => {
           var index = this.teams.findIndex(obj => obj.id==id);
           this.teams.splice(index,1);
         });
     this.displayDelete='none';
   }
}
