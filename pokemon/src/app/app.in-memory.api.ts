import { InMemoryDbService } from 'angular-in-memory-web-api';

export class AppInMemoryApi  implements InMemoryDbService{
      createDb(){
        return {
          'pokemons':[
            {
              'id':1,
              'pokemonName':'Pikachu',
              'Master':'Jefferson Medina'
            },
            {
              'id':2,
              'pokemonName':'chlorophyll',
              'Master':'Manuela Cedeño'
            }
          ],
          'registers':[
            {
              'id':1,
              'trainerName':'admin',
              'email':'jeffersonmedina88@hotmail.com',
              'password':'DFD42866$Dasddff'
            }
          ]

        }
    }
}
