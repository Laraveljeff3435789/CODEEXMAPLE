import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Pokemon } from '../shared/pokemon';
import { Observable , from } from 'rxjs';
import { tap , distinct , toArray} from 'rxjs/operators';
import  * as  pokemonsActions from '../store/actions/pokemons.actions';
import { getAllPokemons  , getPokemon } from '../store/reducers/pokemons.reducers';
import {  GetPokemon } from '../store/actions/pokemons.actions';
import {NgxPaginationModule} from 'ngx-pagination';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.sass']
})
export class PokemonListComponent implements OnInit {
  title = 'List Of pokemons';
  pokemons : Observable<Pokemon[]>;
  listpokemon : Pokemon[];
  listpokemonTemp : Pokemon[];
  pokemon:Observable<Pokemon>;
  listImages:string[] = []
  types:string[] = []
  stringSearch:string=''
  p:any;

  constructor(private router:Router ,
              private store: Store<AppState>) { }

  ngOnInit() {
    this.pokemons = this.store.select(getAllPokemons);
    this.pokemons.subscribe( data =>{
        this.listpokemon=data['results'];
        this.listpokemonTemp = this.listpokemon;
        for (let entry in this.listpokemon) {
          if(+entry==0)continue;
          this.store.dispatch(new GetPokemon(+entry))
          this.pokemon=this.store.select(getPokemon);
          this.pokemon.subscribe( data => {
              if(data != null){
                if(!this.listImages.includes(data['sprites']['front_default'])){
                  this.listImages.push(data['sprites']['front_default'])
                  this.types.push(data['types'][0]['type']['name'])
                }
              }
          })
       }
    });
  }

  setImage(index:number){
     let res =this.listImages[index]
     if(res== undefined) return

     index++
     let data=this.listImages.filter(val=> {
        return val.substr(val.lastIndexOf('/') + 1) == index.toString()+'.png';
     })
    return data;
  }

  setType(index:string){
    return this.types[index];
  }

  OnClickRouter(url:string){
    let res= url.substring(0, url.length - 1);
    let lastURLSegment = res.substr(res.lastIndexOf('/') + 1);
    let routerPath = '/pokemons/detail/'+lastURLSegment;
    this.router.navigate([routerPath]);
  }

  logOut(){
       localStorage.removeItem('userValid')
       window.location.href = "/registers";
  }

  search(){
    let res=this.listpokemon.filter(val => {
        return val.name.includes(this.stringSearch) == true;
    })
    this.listpokemon = res
  }

  noFilter(){
    this.listpokemon =  this.listpokemonTemp
  }

}
