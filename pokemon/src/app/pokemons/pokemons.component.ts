import { Component , OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '../app.state';
import { GetAllPokemons } from './store/actions/pokemons.actions';
import {
  isCreated ,
  isUpdated ,
  isDeleted ,
  getDeleteError ,
  getUpdateError ,
  getPokemonsError } from './store/reducers/pokemons.reducers';

@Component({
  selector:'app-pokemons',
  template:`
  <router-outlet></router-outlet>`,
  styleUrls:['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit {

    constructor(private router: Router , private store :Store<AppState>){
    }

    ngOnInit(){
      // subscriptions when success or error action
      this.store.select(getPokemonsError).subscribe((error) => this.loadingError(error));
      this.store.dispatch(new GetAllPokemons());
    }
    /**
     * Display error message if load of pokemons fails
     */
    loadingError(error) {
      if (error) {
        alert('Error while loading the list of pokemons');
      }
    }

}
