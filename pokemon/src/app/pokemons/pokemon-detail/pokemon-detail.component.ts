import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Pokemon } from '../shared/pokemon';
import { getPokemon } from '../store/reducers/pokemons.reducers';
import {  Observable  } from 'rxjs';
import {  GetPokemon } from '../store/actions/pokemons.actions';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.sass']
})
export class PokemonDetailComponent implements OnInit {

  pokemon:Observable<Pokemon>;

  constructor(private route: ActivatedRoute ,
              private store: Store<AppState>){
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
        this.store.dispatch(new GetPokemon(+params['id']))
    });

    this.pokemon=this.store.select(getPokemon);
  }
}
