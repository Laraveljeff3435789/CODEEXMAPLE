import { NgModule   } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';

//components
import { PokemonsComponent } from './pokemons.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonCreateComponent } from './pokemon-create/pokemon-create.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { PokemonEditComponent } from './pokemon-edit/pokemon-edit.component';

import { AuthGuardService as AuthGuard } from '../shared/utils/auth-guard.service';
import { AuthService } from '../shared/utils/auth.service';


const pokemonsRoutes : Routes  =  <Routes>[{
  path:'',
  component :PokemonsComponent ,canActivate: [AuthGuard],
  children:[
    { path:'' , component:  PokemonListComponent ,canActivate: [AuthGuard]},
    { path:'detail/:id' , component: PokemonDetailComponent ,canActivate: [AuthGuard]},
    { path:'create' , component:  PokemonCreateComponent ,canActivate: [AuthGuard]},
    { path:'edit/:id',  component: PokemonEditComponent ,canActivate: [AuthGuard]}
  ]
}];

@NgModule({
  imports:[
    RouterModule.forChild(pokemonsRoutes)
  ],
  exports:[RouterModule]
})
export class PokemonsRoutingModule {
}

export const pokemonsRoutedComponents = [
  PokemonsComponent,
  PokemonListComponent,
  PokemonCreateComponent,
  PokemonDetailComponent,
  PokemonEditComponent
]
