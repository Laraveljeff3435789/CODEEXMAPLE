import { Action } from '@ngrx/store';
import { Pokemon } from '../../shared/pokemon';

export const GET_POKEMONS = '[All] Pokemons';
export const GET_POKEMONS_SUCESSS ='[All]  Pokemons Success';
export const GET_POKEMONS_ERROR ='[All] Pokemons Error';

export const GET_POKEMON = '[GET] Pokemon';
export const GET_POKEMON_SUCCESS = '[GET] Pokemon Succes';
export const GET_POKEMON_ERROR = '[GET] Pokemon Error';

export const CREATE_POKEMON ='[Create] Pokemon';
export const CREATE_POKEMON_SUCCESS = '[Create] Pokemon Sucess';
export const CREATE_POKEMON_ERROR = '[Create] Pokemon Error';

export const UPDATE_POKEMON = '[Update] Pokemon';
export const UPDATE_POKEMON_SUCCESS = '[Update] Pokemon Success';
export const UPDATE_POKEMON_ERROR = '[Update] Pokemon Error';

export const DELETE_POKEMON = '[Delete] Pokemon';
export const DELETE_POKEMON_SUCCESS = '[Delete] Pokemon Success';
export const DELETE_POKEMON_ERROR = '[Delete] Pokemon Error';

//List Pokemons

export class GetAllPokemons implements Action {
    readonly type = GET_POKEMONS;
}

export class GetAllPokemonsSuccess implements Action {
    readonly type  = GET_POKEMONS_SUCESSS;
    constructor(public payload: Pokemon[]){}
}

export class GetAllPokemonsError implements Action {
    readonly type = GET_POKEMONS_ERROR;
    constructor(public payload: Error){}
}

//Get Pokemon by Id

export class GetPokemon implements Action {
    readonly type = GET_POKEMON;
    constructor(public payload: number){}
}

export class GetPokemonSuccess implements Action {
  readonly  type = GET_POKEMON_SUCCESS;
  constructor(public payload: Pokemon){}
}

export class  GetPokemonError implements Action {
  readonly type =  GET_POKEMON_ERROR;
  constructor(public payload: Error){}
}

//Add Pokemon

export class AddPokemon implements Action {
    readonly type = CREATE_POKEMON;

    constructor(public payload: Pokemon){}
}

export class AddPokemonSuccess implements Action {
    readonly type = CREATE_POKEMON_SUCCESS;

    constructor(public payload: number) {}
}

export class AddPokemonError implements Action {
    readonly type = CREATE_POKEMON_ERROR;

    constructor(public payload: Error){}
}

//Update Pokemon

export class UpdatePokemon implements Action {
    readonly type =  UPDATE_POKEMON;

    constructor(public payload:Pokemon){}
}

export class UpdatePokemonSuccess implements Action {
    readonly type = UPDATE_POKEMON_SUCCESS;
}

export class UpdatePokemonError implements Action {
    readonly type = UPDATE_POKEMON_ERROR;

    constructor(public payload: Error){}
}


//Delete Pokemon

export class DeletePokemon implements Action {
  readonly  type = DELETE_POKEMON;

  constructor(public payload: number){}
}

export class DeletePokemonSuccess implements Action{
  readonly type = DELETE_POKEMON_SUCCESS;

  constructor(public payload: Pokemon){}
}

export class DeletePokemonError implements Action {
  readonly type = DELETE_POKEMON_ERROR;

  constructor(public payload:Error){}
}
