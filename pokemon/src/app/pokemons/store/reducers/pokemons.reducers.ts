import {  AppAction } from '../../../app.action';
import { createFeatureSelector , createSelector } from '@ngrx/store';
import { Pokemon } from '../../shared/Pokemon';
import  * as pokemonActions from '../actions/pokemons.actions';
import { RouterReducerState} from '@ngrx/router-store';

export interface State {
  data:Pokemon[];
  selected:Pokemon;
  action:string;
  done:boolean;
  error?:Error;
}

const initialState: State  = {
  data:[],
  selected:null,
  action:null,
  done:false,
  error:null
}

export function reducer (state = initialState , action :AppAction){
    switch(action.type){
      case pokemonActions.GET_POKEMONS:
        return {
          ...state,
          action: pokemonActions.GET_POKEMONS,
          done:false,
          selected:null,
          error:null
        }
      case pokemonActions.GET_POKEMONS_SUCESSS:
        return {
          ...state,
          data:action.payload,
          done:true,
          selected:null,
          error:null
        }
      case pokemonActions.GET_POKEMONS_ERROR:
        return {
          ...state,
          done:true,
          selected:null,
          error:action.payload
        }
      /*Get Actions By Id*/
      case pokemonActions.GET_POKEMON:
          return {
          ...state,
            action: pokemonActions.GET_POKEMON,
            done: false,
            selected: null,
            error:  null
          }
      case pokemonActions.GET_POKEMON_SUCCESS:
        return {
            ...state,
            selected: action.payload,
            done: true,
            error:  null
        }
      case pokemonActions.GET_POKEMON_ERROR:
        return {
          ...state,
          selected: null,
          done: true,
          error:  action.payload
        }
      case pokemonActions.CREATE_POKEMON:
        return {
          ...state,
          selected: action.payload,
          action: pokemonActions.CREATE_POKEMON,
          done: false,
          error:  null
        }
      case pokemonActions.CREATE_POKEMON_SUCCESS:
        const newPokemon = {
          ...state.selected,
          id: action.payload
        }
        const data = [
          ...state.data,
          newPokemon
        ]
        return {
          ...state,
          data,
          selected: null,
          done: true,
          error:null
        }
     case pokemonActions.CREATE_POKEMON_ERROR:
          return {
            ...state,
            selected: null,
            done: true,
            error: action.payload
          }
    case pokemonActions.UPDATE_POKEMON:
          return {
            ...state,
            selected: action.payload,
            action: pokemonActions.UPDATE_POKEMON,
            done: false,
            error: null
          }
    case  pokemonActions.UPDATE_POKEMON_SUCCESS:
            const index = state
            .data
            .findIndex(h => h.id === state.selected.id);
            if (index >= 0) {
              const data = [
                ...state.data.slice(0, index),
                state.selected,
                ...state.data.slice(index + 1)
              ];
              return {
                ...state,
                data,
                done: true,
                selected: null,
                error: null
              };
            }
            return state;
      case pokemonActions.UPDATE_POKEMON_ERROR:
          return {
            ...state,
            done: true,
            selected : null ,
            error: action.payload
          }
      case pokemonActions.DELETE_POKEMON:
          const selected = state.data.find(h => h.id === action.payload)
          return  {
            ...state,
            selected,
            action: pokemonActions.DELETE_POKEMON,
            done: false,
            error:null
          }
      case pokemonActions.DELETE_POKEMON_SUCCESS:
        {
          const data = state.data.filter( h => h.id !== state.selected.id)
          return {
            ...state,
            data,
            selected: null,
            error: null,
            done: true
          }
        }
      case pokemonActions.DELETE_POKEMON_ERROR:
          return {
            ...state,
            selected: null,
            done:true,
            error:action.payload
          }
      }
    return state;
}

export const getPokemonsState = createFeatureSelector < State > ('pokemons');
export const getAllPokemons = createSelector( getPokemonsState , (state: State ) => state.data);
export const getPokemon = createSelector( getPokemonsState , ( state : State ) => {
  if(state.action === pokemonActions.GET_POKEMON && state.done){
    return state.selected;
  } else{
    return null;
  }
});

export const isCreated =  createSelector( getPokemonsState , ( state: State ) =>
  state.action === pokemonActions.CREATE_POKEMON && state.done && !state.error);

export const isUpdated = createSelector(getPokemonsState , (state : State ) =>
    state.action === pokemonActions.UPDATE_POKEMON && state.done && !state.error);

export const isDeleted = createSelector(getPokemonsState , (state: State) =>
      state.action === pokemonActions.DELETE_POKEMON && state.done && !state.error);

export const getCreateError = createSelector( getPokemonsState , (state: State) => {
    return state.action === pokemonActions.CREATE_POKEMON
           ? state.error
           : null;
});

export const getDeleteError = createSelector(getPokemonsState, (state: State) => {
    return state.action === pokemonActions.DELETE_POKEMON
      ? state.error
     : null;
});

export const getUpdateError = createSelector(getPokemonsState, (state: State) => {
    return state.action === pokemonActions.UPDATE_POKEMON
      ? state.error
     : null;
});

export const getPokemonsError = createSelector(getPokemonsState, (state: State) => {
  return state.action === pokemonActions.GET_POKEMONS
    ? state.error
   : null;
});

export const getPokemonError = createSelector(getPokemonsState, (state: State) => {
    return state.action === pokemonActions.GET_POKEMON
      ? state.error
     : null;
});
