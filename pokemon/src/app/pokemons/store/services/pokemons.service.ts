import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap , catchError , retryWhen} from 'rxjs/operators';
import { Pokemon } from '../../shared/pokemon';
import { TraceService } from '../../../shared/utils/traceService';

@Injectable()
export class PokemonsService {
  protected URL='https://pokeapi.co/api/v2/pokemon';

  constructor(private http: HttpClient ,private traceService: TraceService){ }

  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?): Observable<Pokemon[]>{
      return this.http.get<Pokemon[]>(this.URL , { params: params }).pipe(
            tap(_ => this.traceService.log('fetched pokemons')),
            catchError(this.traceService.handleError<Pokemon[]>('findAll', []))
      )
  }
  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Pokemon> {
    return this.http.get<Pokemon>(this.URL + '/' + id).pipe(
      tap(_ => this.traceService.log(`fetched pokemon id=${id}`)),
      catchError(this.traceService.handleError<Pokemon>(`findById id=${id}`))
    )
  }
  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
   public insert(data: Pokemon): Observable<Pokemon>{
     let headers = new HttpHeaders();
     headers = headers.set('Content-Type','application/json; charset=utf-8');
     return this.http.post<Pokemon>(this.URL , data  ,{headers: headers })
     .pipe(
        tap((newPokemon:Pokemon) => this.traceService.log(`pokemon w/ id=${newPokemon.id}`)),
        catchError(this.traceService.handleError<Pokemon>('insert'))
     )
   }
   /**
    * Update specific object into DB
    * @param pokemon the object to be updated
    * @returns gets the response
    */
   public update(pokemon: Pokemon): Observable<Pokemon> {
     let headers = new HttpHeaders();
     headers = headers.set('Content-Type', 'application/json; charset=utf-8');
     return this.http.put<Pokemon>(this.URL + '/' + pokemon.id, pokemon, {headers: headers}).pipe(
       tap(_ => this.traceService.log(`updated pokemon id=${pokemon.id}`)),
      catchError(this.traceService.handleError<any>('update'))
     )
   }
   /**
    * Delete an object by its identifier field
    * @param id the object identifier
    * @returns gets the response
    */
   public delete(id): Observable<Pokemon> {
     return this.http.delete<Pokemon>(this.URL + '/' + id).pipe(
       tap(_ => this.traceService.log(`deleted pokemon id=${id}`)),
       catchError(this.traceService.handleError<Pokemon>('delete'))
     )
   }
}
