import { Injectable } from '@angular/core';
import { Actions , Effect , ofType } from '@ngrx/effects';
import  * as pokemonActions from '../actions/pokemons.actions';
import { Observable , of } from 'rxjs';
import { Action } from '@ngrx/store';
import { catchError , map , mergeMap , retryWhen  } from 'rxjs/operators';
import {
GetAllPokemons ,
GetAllPokemonsSuccess ,
GetAllPokemonsError ,
AddPokemon ,
AddPokemonSuccess ,
AddPokemonError,
UpdatePokemon,
UpdatePokemonSuccess,
UpdatePokemonError,
GetPokemon,
GetPokemonSuccess,
GetPokemonError,
DeletePokemon,
DeletePokemonSuccess,
DeletePokemonError
}
from '../actions/pokemons.actions';
import { PokemonsService } from '../services/pokemons.service';
import { Pokemon } from '../../shared/pokemon';

@Injectable()
export class PokemonEffects {
  constructor(private actions$:Actions , private svc:PokemonsService){}

  @Effect()
  public getAllpokemons$ : Observable<Action> = this.actions$.pipe(
      ofType<GetAllPokemons>(pokemonActions.GET_POKEMONS),
        mergeMap((action:GetAllPokemons) =>
          this.svc.findAll().pipe(
              map((pokemons: Pokemon[]) => new GetAllPokemonsSuccess(pokemons)),
                catchError(err => of(new GetAllPokemonsError(err)))
          )
      )
  );

  @Effect()
  public getPokemon$ : Observable<Action> =  this.actions$.pipe(
      ofType<GetPokemon>(pokemonActions.GET_POKEMON),
        mergeMap((action: GetPokemon) =>
          this.svc.findById(action.payload).pipe(
            map((pokemon: Pokemon) => new GetPokemonSuccess(pokemon)),
              catchError(err => of(new GetPokemonError(err)))
        )
    )
  );

  @Effect()
  public createPokemon$ :  Observable<Action> = this.actions$.pipe(
      ofType<AddPokemon>(pokemonActions.CREATE_POKEMON),
        mergeMap((action:AddPokemon) =>
          this.svc.insert(action.payload).pipe(
              map((pokemon:Pokemon) => new AddPokemonSuccess(pokemon.id)),
              catchError(err => of(new AddPokemonError(err)))
            )
        )
  );

  @Effect()
  public updatePokemon : Observable<Action> = this.actions$.pipe(
      ofType<UpdatePokemon>(pokemonActions.UPDATE_POKEMON),
        mergeMap((action:UpdatePokemon) =>
          this.svc.update(action.payload).pipe(
            map((pokemon:Pokemon) => new UpdatePokemonSuccess()),
            catchError(err => of(new UpdatePokemonError(err)))
        )
      )
  );

  @Effect()
  public deletePokemon: Observable<Action> =  this.actions$.pipe(
      ofType<DeletePokemon>(pokemonActions.DELETE_POKEMON),
        mergeMap((action:DeletePokemon)  =>
          this.svc.delete(action.payload).pipe(
            map((pokemon: Pokemon) => new DeletePokemonSuccess(pokemon)),
            catchError(err => of(new DeletePokemonError(err)))
          )
      )
  );
}
