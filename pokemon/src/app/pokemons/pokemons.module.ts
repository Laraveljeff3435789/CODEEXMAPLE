import { NgModule } from '@angular/core';
import { PokemonsService } from './store/services/pokemons.service';
import { pokemonsRoutedComponents , PokemonsRoutingModule} from './pokemons-routing.module';
import { SharedModule } from '../shared/shared.module';

import { StoreModule , ActionReducerMap } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';

import { EffectsModule } from '@ngrx/effects';
import { PokemonEffects } from './store/effects/pokemons.effects';
import  * as pokemonsReducers from './store/reducers/pokemons.reducers';
import { TraceService } from '../shared/utils/traceService';

//Grafics components
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

//AuthGuard service
import {AuthService} from '../shared/utils/auth.service';
import {AuthGuardService} from '../shared/utils/auth-guard.service';
import {TokenInterceptor, ErrorInterceptor } from '../shared/utils/token.interceptor';

import {NgxPaginationModule} from 'ngx-pagination';

export const reducers: ActionReducerMap<any> = {
  pokemons:pokemonsReducers.reducer,
  router: routerReducer
}

@NgModule({
  imports:[
    SharedModule,
    PokemonsRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([PokemonEffects]),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router' // name of reducer key
    }),
    StoreDevtoolsModule.instrument({
      maxAge:25
    })
  ],
  declarations:[pokemonsRoutedComponents],
  providers:[ PokemonsService , TraceService, AuthService, AuthGuardService,TokenInterceptor,ErrorInterceptor ]
})
export class PokemonsModule {

}
