import * as fromPokemons from './pokemons/store/reducers/pokemons.reducers';
import * as fromRegisters from './registers/store/reducers/registers.reducers';

export interface AppState {
  pokemons:fromPokemons.State;
  registers:fromRegisters.State;
}
