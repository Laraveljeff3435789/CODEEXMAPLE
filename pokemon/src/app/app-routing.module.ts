import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

//Components
import { PageNotFoundComponent } from './shared/not-found/not-found.component';

const routes:Routes = [
  { path:'' , redirectTo:'/registers' , pathMatch:'full'},
  {
    path:'registers',
    loadChildren:'./registers/registers.module#RegistersModule'
  },
  {
    path:'pokemons',
    loadChildren:'./pokemons/pokemons.module#PokemonsModule'
  }
];

@NgModule({
  imports:[RouterModule.forRoot(routes,{useHash:true})],
  exports:[RouterModule]
})
export class AppRoutingModule {

}
