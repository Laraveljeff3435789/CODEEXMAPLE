import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Register } from '../shared/register';
import { getRegister } from '../store/reducers/registers.reducers';
import {  Observable  } from 'rxjs';
import {  GetRegister } from '../store/actions/registers.actions';

@Component({
  selector: 'app-register-detail',
  templateUrl: './register-detail.component.html',
  styleUrls: ['./register-detail.component.sass']
})
export class RegisterDetailComponent implements OnInit {

  register:Observable<Register>;

  constructor(private route: ActivatedRoute ,
              private store: Store<AppState>){ }

  ngOnInit() {
    this.route.params.subscribe( params => {
        this.store.dispatch(new GetRegister(+params['id']))
    });

    this.register=this.store.select(getRegister);
    this.register.subscribe( data => {
          console.log(data)
    })
  }

}
