import { Component , OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '../app.state';
import { GetAllRegisters } from './store/actions/registers.actions';
import {
  isCreated ,
  isUpdated ,
  isDeleted ,
  getDeleteError ,
  getUpdateError ,
  getRegistersError } from './store/reducers/registers.reducers';

@Component({
  selector:'app-registers',
  template:`
  <router-outlet></router-outlet>`,
  styleUrls:['./registers.component.css']
})
export class RegisterComponent implements OnInit {

    constructor(private router: Router , private store :Store<AppState>){ }

    ngOnInit(){
      // subscriptions when success or error action
     this.store.select(getRegistersError).subscribe((error) => this.loadingError(error));
     this.store.dispatch(new GetAllRegisters());
    }

    /**
     * Display error message if load of registers fails
     */
    loadingError(error) {
      if (error) {
        alert('Error while loading the list of registers');
      }
    }
}
