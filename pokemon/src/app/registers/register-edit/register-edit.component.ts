import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Observable , from } from 'rxjs';
import { GetRegister , UpdateRegister } from '../store/actions/registers.actions';
import { getRegister , getAllRegisters } from '../store/reducers/registers.reducers';
import {  Register } from '../shared/register';

@Component({
  selector: 'app-register-edit',
  templateUrl: './register-edit.component.html',
  styleUrls: ['./register-edit.component.sass']
})
export class RegisterEditComponent implements OnInit {
  register:Register;
  registers : Observable<Register[]>;

  constructor(private route:ActivatedRoute, private store:Store<AppState>) { }

  ngOnInit() {
    this.route.params.subscribe( params =>{
        this.store.dispatch(new GetRegister(+params['id']))
    })

    this.store.select(getRegister).subscribe( register => {
        if(register != null){
            this.register = register
        }
    });
  }


  updateRegister(){
    console.log('updateRegister')
    this.register.trainerName ='Change Name Training';
    this.register.password ='345fggh';
    this.store.dispatch(new UpdateRegister(this.register));

    this.registers = this.store.select(getAllRegisters);
    this.registers.subscribe( data =>{
        console.log(data)
    });
  }

}
