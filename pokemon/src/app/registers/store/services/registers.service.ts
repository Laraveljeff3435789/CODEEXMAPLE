import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap , catchError , retryWhen} from 'rxjs/operators';
import { Register } from '../../shared/register';
import { TraceService } from '../../../shared/utils/traceService';

@Injectable()
export class RegistersService {
  protected URL ='http://localhost:3000/api/registers';

  constructor(private http: HttpClient ,private traceService: TraceService){ }

  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?): Observable<Register[]>{
      return this.http.get<Register[]>(this.URL , { params: params }).pipe(
            tap(_ => this.traceService.log('fetched registers')),
            catchError(this.traceService.handleError<Register[]>('findAll', []))
      )
  }
  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Register> {
    return this.http.get<Register>(this.URL + '/' + id).pipe(
      tap(_ => this.traceService.log(`fetched register id=${id}`)),
      catchError(this.traceService.handleError<Register>(`findById id=${id}`))
    )
  }

  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
   public insert(data: Register): Observable<Register>{
     let headers = new HttpHeaders();
     headers = headers.set('Content-Type','application/json; charset=utf-8');
     return this.http.post<Register>(this.URL , data  ,{headers: headers })
     .pipe(
        tap((newRegister:Register) => this.traceService.log(`register w/ id=${newRegister.id}`)),
        catchError(this.traceService.handleError<Register>('insert'))
     )
   }

   /**
    * Update specific object into DB
    * @param register the object to be updated
    * @returns gets the response
    */
   public update(register: Register): Observable<Register> {
     let headers = new HttpHeaders();
     headers = headers.set('Content-Type', 'application/json; charset=utf-8');
     return this.http.put<Register>(this.URL + '/' + register.id, register, {headers: headers}).pipe(
       tap(_ => this.traceService.log(`updated register id=${register.id}`)),
      catchError(this.traceService.handleError<any>('update'))
     )
   }

   /**
    * Delete an object by its identifier field
    * @param id the object identifier
    * @returns gets the response
    */
   public delete(id): Observable<Register> {
     return this.http.delete<Register>(this.URL + '/' + id).pipe(
       tap(_ => this.traceService.log(`deleted register id=${id}`)),
       catchError(this.traceService.handleError<Register>('delete'))
     )
   }

}
