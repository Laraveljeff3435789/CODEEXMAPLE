import {  AppAction } from '../../../app.action';
import { createFeatureSelector , createSelector } from '@ngrx/store';
import { Register } from '../../shared/Register';
import  * as registerActions from '../actions/registers.actions';
import { RouterReducerState} from '@ngrx/router-store';

export interface State {
  data:Register[];
  selected:Register;
  action:string;
  done:boolean;
  error?:Error;
}

const initialState: State  = {
  data:[],
  selected:null,
  action:null,
  done:false,
  error:null
}

export function reducer (state = initialState , action :AppAction){
    switch(action.type){
      case registerActions.GET_REGISTERS:
        return {
          ...state,
          action: registerActions.GET_REGISTERS,
          done:false,
          selected:null,
          error:null
        }
      case registerActions.GET_REGISTERS_SUCESSS:
        return {
          ...state,
          data:action.payload,
          done:true,
          selected:null,
          error:null
        }
      case registerActions.GET_REGISTERS_ERROR:
        return {
          ...state,
          done:true,
          selected:null,
          error:action.payload
        }
      /*Get Actions By Id*/
      case registerActions.GET_REGISTER:
          return {
          ...state,
            action: registerActions.GET_REGISTER,
            done: false,
            selected: null,
            error:  null
          }
      case registerActions.GET_REGISTER_SUCCESS:
        return {
            ...state,
            selected: action.payload,
            done: true,
            error:  null
        }
      case registerActions.GET_REGISTER_ERROR:
        return {
          ...state,
          selected: null,
          done: true,
          error:  action.payload
        }
      case registerActions.CREATE_REGISTER:
        return {
          ...state,
          selected: action.payload,
          action: registerActions.CREATE_REGISTER,
          done: false,
          error:  null
        }
      case registerActions.CREATE_REGISTER_SUCCESS:
        const newRegister = {
          ...state.selected,
          id: action.payload
        }
        const data = [
          ...state.data,
          newRegister
        ]
        return {
          ...state,
          data,
          selected: null,
          done: true,
          error:null
        }
     case registerActions.CREATE_REGISTER_ERROR:
          return {
            ...state,
            selected: null,
            done: true,
            error: action.payload
          }
    case registerActions.UPDATE_REGISTER:
          return {
            ...state,
            selected: action.payload,
            action: registerActions.UPDATE_REGISTER,
            done: false,
            error: null
          }
    case  registerActions.UPDATE_REGISTER_SUCCESS:
            const index = state
            .data
            .findIndex(h => h.id === state.selected.id);
            if (index >= 0) {
              const data = [
                ...state.data.slice(0, index),
                state.selected,
                ...state.data.slice(index + 1)
              ];
              return {
                ...state,
                data,
                done: true,
                selected: null,
                error: null
              };
            }
            return state;
      case registerActions.UPDATE_REGISTER_ERROR:
          return {
            ...state,
            done: true,
            selected : null ,
            error: action.payload
          }
      case registerActions.DELETE_REGISTER:
          const selected = state.data.find(h => h.id === action.payload)
          return  {
            ...state,
            selected,
            action: registerActions.DELETE_REGISTER,
            done: false,
            error:null
          }
      case registerActions.DELETE_REGISTER_SUCCESS:
        {
          const data = state.data.filter( h => h.id !== state.selected.id)
          return {
            ...state,
            data,
            selected: null,
            error: null,
            done: true
          }
        }
      case registerActions.DELETE_REGISTER_ERROR:
          return {
            ...state,
            selected: null,
            done:true,
            error:action.payload
          }
      }
    return state;
}

export const getRegistersState = createFeatureSelector < State > ('registers');
export const getAllRegisters = createSelector( getRegistersState , (state: State ) => state.data);
export const getRegister = createSelector( getRegistersState , ( state : State ) => {
  if(state.action === registerActions.GET_REGISTER && state.done){
    return state.selected;
  } else{
    return null;
  }
});

export const isCreated =  createSelector( getRegistersState , ( state: State ) =>
  state.action === registerActions.CREATE_REGISTER && state.done && !state.error);

export const isUpdated = createSelector(getRegistersState , (state : State ) =>
    state.action === registerActions.UPDATE_REGISTER && state.done && !state.error);

export const isDeleted = createSelector(getRegistersState , (state: State) =>
    state.action === registerActions.DELETE_REGISTER && state.done && !state.error);

export const getCreateError = createSelector( getRegistersState , (state: State) => {
        return state.action === registerActions.CREATE_REGISTER
            ? state.error
            : null;
});

export const getDeleteError = createSelector(getRegistersState, (state: State) => {
    return state.action === registerActions.DELETE_REGISTER
      ? state.error
     : null;
});

export const getUpdateError = createSelector(getRegistersState, (state: State) => {
    return state.action === registerActions.UPDATE_REGISTER
      ? state.error
     : null;
});


export const getRegistersError = createSelector(getRegistersState, (state: State) => {
  return state.action === registerActions.GET_REGISTERS
    ? state.error
   : null;
});

export const getRegisterError = createSelector(getRegistersState, (state: State) => {
    return state.action === registerActions.GET_REGISTER
      ? state.error
     : null;
});
