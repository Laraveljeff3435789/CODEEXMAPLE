import { Action } from '@ngrx/store';
import { Register } from '../../shared/register';

export const GET_REGISTERS = '[All] Registers';
export const GET_REGISTERS_SUCESSS ='[All]  Registers Success';
export const GET_REGISTERS_ERROR ='[All] Registers Error';

export const GET_REGISTER = '[GET] Register';
export const GET_REGISTER_SUCCESS = '[GET] Register Succes';
export const GET_REGISTER_ERROR = '[GET] Register Error';

export const CREATE_REGISTER ='[Create] Register';
export const CREATE_REGISTER_SUCCESS = '[Create] Register Sucess';
export const CREATE_REGISTER_ERROR = '[Create] Register Error';

export const UPDATE_REGISTER = '[Update] Register';
export const UPDATE_REGISTER_SUCCESS = '[Update] Register Success';
export const UPDATE_REGISTER_ERROR = '[Update] Register Error';

export const DELETE_REGISTER = '[Delete] Register';
export const DELETE_REGISTER_SUCCESS = '[Delete] Register Success';
export const DELETE_REGISTER_ERROR = '[Delete] Register Error';

//List Registers

export class GetAllRegisters implements Action {
    readonly type = GET_REGISTERS;
}

export class GetAllRegistersSuccess implements Action {
    readonly type  = GET_REGISTERS_SUCESSS;
    constructor(public payload: Register[]){}
}

export class GetAllRegistersError implements Action {
    readonly type = GET_REGISTERS_ERROR;
    constructor(public payload: Error){}
}

//Get Register by Id

export class GetRegister implements Action {
    readonly type = GET_REGISTER;
    constructor(public payload: number){}
}

export class GetRegisterSuccess implements Action {
  readonly  type = GET_REGISTER_SUCCESS;
  constructor(public payload: Register){}
}

export class  GetRegisterError implements Action {
  readonly type =  GET_REGISTER_ERROR;
  constructor(public payload: Error){}
}

//Add Register

export class AddRegister implements Action {
    readonly type = CREATE_REGISTER;
    constructor(public payload: Register){}
}

export class AddRegisterSuccess implements Action {
    readonly type = CREATE_REGISTER_SUCCESS;
    constructor(public payload: number) {}
}

export class AddRegisterError implements Action {
    readonly type = CREATE_REGISTER_ERROR;
    constructor(public payload: Error){}
}

//Update Register

export class UpdateRegister implements Action {
    readonly type =  UPDATE_REGISTER;
    constructor(public payload:Register){}
}

export class UpdateRegisterSuccess implements Action {
    readonly type = UPDATE_REGISTER_SUCCESS;
}

export class UpdateRegisterError implements Action {
    readonly type = UPDATE_REGISTER_ERROR;
    constructor(public payload: Error){}
}


//Delete Register
export class DeleteRegister implements Action {
  readonly  type = DELETE_REGISTER;
  constructor(public payload: number){}
}

export class DeleteRegisterSuccess implements Action{
  readonly type = DELETE_REGISTER_SUCCESS;
  constructor(public payload: Register){}
}

export class DeleteRegisterError implements Action {
  readonly type = DELETE_REGISTER_ERROR;
  constructor(public payload:Error){}
}
