import { Injectable } from '@angular/core';
import { Actions , Effect , ofType } from '@ngrx/effects';
import  * as registerActions from '../actions/registers.actions';
import { Observable , of } from 'rxjs';
import { Action } from '@ngrx/store';
import { catchError , map , mergeMap , retryWhen  } from 'rxjs/operators';
import {
GetAllRegisters ,
GetAllRegistersSuccess ,
GetAllRegistersError ,
AddRegister ,
AddRegisterSuccess ,
AddRegisterError,
UpdateRegister,
UpdateRegisterSuccess,
UpdateRegisterError,
GetRegister,
GetRegisterSuccess,
GetRegisterError,
DeleteRegister,
DeleteRegisterSuccess,
DeleteRegisterError
}
from '../actions/registers.actions';
import { RegistersService } from '../services/registers.service';
import { Register } from '../../shared/register';

@Injectable()
export class RegisterEffects {
  constructor(private actions$:Actions , private svc:RegistersService){}

  @Effect()
  public getAllregisters$ : Observable<Action> = this.actions$.pipe(
      ofType<GetAllRegisters>(registerActions.GET_REGISTERS),
        mergeMap((action:GetAllRegisters) =>
          this.svc.findAll().pipe(
              map((registers: Register[]) => new GetAllRegistersSuccess(registers)),
                catchError(err => of(new GetAllRegistersError(err)))
          )
      )
  );

  @Effect()
  public getRegister$ : Observable<Action> =  this.actions$.pipe(
      ofType<GetRegister>(registerActions.GET_REGISTER),
        mergeMap((action: GetRegister) =>
          this.svc.findById(action.payload).pipe(
            map((register: Register) => new GetRegisterSuccess(register)),
              catchError(err => of(new GetRegisterError(err)))
        )
    )
  );

  @Effect()
  public createRegister$ :  Observable<Action> = this.actions$.pipe(
      ofType<AddRegister>(registerActions.CREATE_REGISTER),
        mergeMap((action:AddRegister) =>
          this.svc.insert(action.payload).pipe(
              map((register:Register) => new AddRegisterSuccess(register.id)),
              catchError(err => of(new AddRegisterError(err)))
            )
        )
  );

  @Effect()
  public updateRegister : Observable<Action> = this.actions$.pipe(
      ofType<UpdateRegister>(registerActions.UPDATE_REGISTER),
        mergeMap((action:UpdateRegister) =>
          this.svc.update(action.payload).pipe(
            map((register:Register) => new UpdateRegisterSuccess()),
            catchError(err => of(new UpdateRegisterError(err)))
        )
      )
  );

  @Effect()
  public deleteRegister: Observable<Action> =  this.actions$.pipe(
      ofType<DeleteRegister>(registerActions.DELETE_REGISTER),
        mergeMap((action:DeleteRegister)  =>
          this.svc.delete(action.payload).pipe(
            map((register: Register) => new DeleteRegisterSuccess(register)),
            catchError(err => of(new DeleteRegisterError(err)))
          )
      )
  );

}
