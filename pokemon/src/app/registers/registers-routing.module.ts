import { NgModule   } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';

//components
import { RegisterComponent } from './registers.component';
import { RegisterListComponent } from './register-list/register-list.component';
import { RegisterCreateComponent } from './register-create/register-create.component';
import { RegisterDetailComponent } from './register-detail/register-detail.component';
import { RegisterEditComponent } from './register-edit/register-edit.component';

import { AuthGuardService as AuthGuard } from '../shared/utils/auth-guard.service';
import { AuthService } from '../shared/utils/auth.service';

const registersRoutes : Routes  =  <Routes>[{
  path:'',
  component :RegisterComponent,
  children:[
    { path:'' , component:  RegisterListComponent },
    { path:'detail/:id' , component: RegisterDetailComponent  },
    { path:'create' , component:  RegisterCreateComponent },
    { path:'edit/:id',  component: RegisterEditComponent}
  ]
}];

@NgModule({
  imports:[
    RouterModule.forChild(registersRoutes)
  ],
  exports:[RouterModule]
})
export class RegistersRoutingModule {
}

export const registersRoutedComponents = [
  RegisterComponent,
  RegisterListComponent,
  RegisterCreateComponent,
  RegisterDetailComponent,
  RegisterEditComponent
]
