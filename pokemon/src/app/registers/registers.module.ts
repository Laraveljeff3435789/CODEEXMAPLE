import { NgModule } from '@angular/core';
import { RegistersService } from './store/services/registers.service';
import { registersRoutedComponents , RegistersRoutingModule} from './registers-routing.module';
import { SharedModule } from '../shared/shared.module';

import { StoreModule , ActionReducerMap } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';

import { EffectsModule } from '@ngrx/effects';
import { RegisterEffects } from './store/effects/registers.effects';
import  * as registersReducers from './store/reducers/registers.reducers';
import { TraceService } from '../shared/utils/traceService';

//Grafics components
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { AppInMemoryApi } from '../app.in-memory.api';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

//AuthGuard service
import {AuthService} from '../shared/utils/auth.service';
import {AuthGuardService} from '../shared/utils/auth-guard.service';
import {TokenInterceptor, ErrorInterceptor } from '../shared/utils/token.interceptor';

export const reducers: ActionReducerMap<any> = {
  registers:registersReducers.reducer,
  router: routerReducer
}

@NgModule({
  imports:[
    SharedModule,
    HttpClientInMemoryWebApiModule.forRoot(AppInMemoryApi),
    RegistersRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([RegisterEffects]),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgSelectModule,
    FormsModule,
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router' // name of reducer key
    }),
    StoreDevtoolsModule.instrument({
      maxAge:25
    })
  ],
  declarations:[registersRoutedComponents],
  providers:[ RegistersService , TraceService, AuthService, AuthGuardService,TokenInterceptor,ErrorInterceptor ]
})
export class RegistersModule {

}
