import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Register } from '../shared/register';
import { Observable , from } from 'rxjs';
import { tap , distinct , toArray} from 'rxjs/operators';
import  * as  registersActions from '../store/actions/registers.actions';
import { getAllRegisters } from '../store/reducers/registers.reducers';
import swal from'sweetalert2';

@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.sass']
})
export class RegisterListComponent implements OnInit {
  title = 'List Of registers';
  registers : Observable<Register[]>;
  email:string='';
  password:string;

  constructor(private router:Router , private store: Store<AppState>) { }

  ngOnInit() {
    this.registers = this.store.select(getAllRegisters);
  }

  validateLogin(){
    this.registers.subscribe( data =>{
        let res=data.filter( val=>{
          return (val.email == this.email && val.password == this.password) == true;
        })

        if(res.length == 1){
          localStorage.setItem("userValid", "DEFDKVCasxc123dqwe2442$");
          this.router.navigate(['/pokemons']);
          return;
        }

        swal.fire(
          'Alerta',
          'Invalid user data',
          'warning'
        )
    });
  }

}
