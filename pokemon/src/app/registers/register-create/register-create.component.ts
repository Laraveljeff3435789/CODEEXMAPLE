import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { AddRegister } from '../store/actions/registers.actions';
import { Observable , from } from 'rxjs';
import { Register } from  '../shared/register';
import { getAllRegisters } from '../store/reducers/registers.reducers';
import  * as  registersActions from '../store/actions/registers.actions';
import swal from'sweetalert2';

@Component({
  selector: 'app-register-create',
  templateUrl: './register-create.component.html',
  styleUrls: ['./register-create.component.sass']
})
export class RegisterCreateComponent implements OnInit {
  register:Register = new Register();
  registers : Observable<Register[]>;
  conpassword:string = '';

  constructor(private router:Router , private store: Store<AppState>) { }

  ngOnInit() {
  }

  AddRegister(){
    //Validation trainer name
    if(this.register.trainerName == '' || !this.register.hasOwnProperty('trainerName')){
      swal.fire(
        'Alerta',
        'Trainer name is required',
        'warning'
      )
      return;
    }

    //Validation email
    if(this.register.email == '' || !this.register.hasOwnProperty('email')){
      swal.fire(
        'Alerta',
        'Email is required',
        'warning'
      )
      return;
    }

    //Validation is mail valid
    if(!this.validateEmail(this.register.email)){
      swal.fire(
        'Alerta',
        'You must enter a valid email',
        'warning'
      )
      return;
    }

    //Validation password
    if(this.register.password == '' || !this.register.hasOwnProperty('password')){
      swal.fire(
        'Alerta',
        'Password is required',
        'warning'
      )
      return;
    }

    //Validation confirmation password
    if(this.conpassword == '' ){
      swal.fire(
        'Alerta',
        'You must confirm the password',
        'warning'
      )
      return;
    }

    //Validation equals password
    if(this.register.password != this.conpassword){
      swal.fire(
        'Alerta',
        'Both password must be the same',
        'warning'
      )
      return;
    }

    //Validation minimum 8 characters
    if(this.register.password.length < 8 ){
      swal.fire(
        'Alerta',
        'The password must be a minimum of 8 characters',
        'warning'
      )
      return;
    }

    //Validation minimum two capital letters
    if(this.register.password.replace(/[^A-Z]/g, "").length < 2){
      swal.fire(
        'Alerta',
        'The password must have a minimum of two capital letters',
        'warning'
      )
      return;
    }

    //Validation is special characters
    let format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    if(!format.test(this.register.password)){
      swal.fire(
        'Alerta',
        'The password must have at least one special character',
        'warning'
      )
      return;
    }

    //Validation password if have minimum one number
   if(this.register.password.replace(/[^0-9]/g,"").length==0){
     swal.fire(
       'Alerta',
       'The password must have a minimum number',
       'warning'
     )
     return;
   }

    this.store.dispatch(new AddRegister(this.register));
    swal.fire(
        'Bien Hecho',
        'Se registro el usuario',
        'success'
    );

    this.router.navigate(['/registers']);
  }


  //Function validation email
  validateEmail(email:string) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
 }

}
